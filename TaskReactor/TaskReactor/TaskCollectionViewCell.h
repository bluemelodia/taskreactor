//
//  TaskCollectionViewCell.h
//  TaskReactor
//
//  Created by Melanie Lislie Hsu on 8/21/15.
//  Copyright (c) 2015 Melanie Lislie Hsu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TaskCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UITextField *taskName;

@end
