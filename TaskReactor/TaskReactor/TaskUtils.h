//
//  TaskUtils.h
//  TaskReactor
//
//  Created by Melanie Lislie Hsu on 8/21/15.
//  Copyright (c) 2015 Melanie Lislie Hsu. All rights reserved.
//

#import <Foundation/Foundation.h>

#define iDevice UI_USER_INTERFACE_IDIOM()
#define iPad    UIUserInterfaceIdiomPad
#define iPhone  UIUserInterfaceIdiomPhone

#define SYSTEM_VERSION_LESS_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

#define CREATE_NEW_TASK @"create_new_task"

@interface TaskUtils : NSObject

+ (NSDictionary*)readProductDocument:(NSString*)docName;

@end
