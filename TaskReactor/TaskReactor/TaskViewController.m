//
//  TaskViewController.m
//  TaskReactor
//
//  Created by Melanie Lislie Hsu on 8/21/15.
//  Copyright (c) 2015 Melanie Lislie Hsu. All rights reserved.
//

#import <Parse/Parse.h>
#import "NewTaskViewController.h"
#import "TaskViewController.h"
#import "TaskCollectionViewCell.h"
#import "TaskUtils.h"
#import "UIScrollView+EmptyDataSet.h"

@interface TaskViewController () <DZNEmptyDataSetSource, DZNEmptyDataSetDelegate> {
    UIFont *customFont;
    NSMutableArray *reactors;
}

@end

@implementation TaskViewController

@synthesize navBar, colorPalette, collView, plus, segControl;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    // Register cell classes
    UINib *cellNib = [UINib nibWithNibName:@"TaskCollectionViewCell" bundle:nil];
    [collView registerNib:cellNib forCellWithReuseIdentifier:@"TaskCell"];
    
    // Set the empty data set delegate and source
    collView.emptyDataSetSource = self;
    collView.emptyDataSetDelegate = self;
    
    // Flat array
    colorPalette = [[NSMutableArray alloc] init];
    [colorPalette addObjectsFromArray:[NSArray arrayOfColorsWithColorScheme:ColorSchemeComplementary with:[UIColor colorWithRed:173/255.0f green:229/255.0f blue:236/255.0f alpha:1.0] flatScheme:NO]];
    [collView setBackgroundView:[[UIImageView alloc ] initWithImage:[UIImage imageNamed:@"Space"]]];
    collView.contentMode = UIViewContentModeScaleAspectFit;
    
    // Customize navigation bar shadow layer
    navBar.layer.shadowColor = [colorPalette[0] CGColor];
    navBar.layer.shadowOffset = CGSizeMake(1.0f, 1.0f);
    navBar.layer.shadowRadius = 1.0f;
    navBar.layer.shadowOpacity = 1.0f;
    
    [plus setTintColor:colorPalette[0]];
    
    // Register a custom font
    customFont = [UIFont fontWithName:@"Bend2SquaresBRK" size:28];
    
    reactors = [[NSMutableArray alloc] init];
    
    /*PFObject *testObject = [PFObject objectWithClassName:@"TestObject"];
    testObject[@"foo"] = @"bar";
    [testObject saveInBackground];*/
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - DZNEmptyDataSetSource Methods
- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    return nil;
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *text = @"No ongoing reactions.\nWhere is your sense of adventure?";
    UIColor *textColor = [UIColor colorWithRed:173/255.0f green:229/255.0f blue:236/255.0f alpha:1.0];
    
    NSMutableDictionary *attributes = [NSMutableDictionary new];
    [attributes setObject:textColor forKey:NSForegroundColorAttributeName];
    [attributes setObject:customFont forKey:NSFontAttributeName];
    
    return [[NSMutableAttributedString alloc] initWithString:text attributes:attributes];
}

- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView {
    return [UIImage imageNamed:@"UFO"];
}

# pragma mark <UICollectionViewLayout>
// Set the cell size
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGSize cellSize;
    
    if (iDevice == iPhone) {
        cellSize = CGSizeMake(screenWidth/2, screenWidth/2);
    } else {
        cellSize = CGSizeMake(screenWidth/3, screenWidth/3);
    }
    return cellSize;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

// Set layout edges
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

#pragma mark <UICollectionViewDataSource>
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [reactors count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    TaskCollectionViewCell *cell = (TaskCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"TaskCell" forIndexPath:indexPath];
    cell.backgroundColor = [UIColor whiteColor];
    cell.layer.borderColor = [collectionView.backgroundColor CGColor];
    cell.layer.borderWidth = 1.0f;
    // Configure the cell
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"CLICK");
    //TaskCollectionViewCell *cell = (TaskCollectionViewCell *)[self collectionView:collectionView cellForItemAtIndexPath:indexPath];
}


# pragma mark New Task Creation
- (IBAction)onNewTaskPress:(UIButton *)sender {
    NewTaskViewController *newTaskVC;
    if (iDevice == iPhone) {
        newTaskVC = [[NewTaskViewController alloc] init];
        [self presentViewController:newTaskVC animated:YES completion:nil];
    } else {
        
    }
}

@end
