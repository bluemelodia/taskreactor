//
//  TaskCollectionViewCell.m
//  TaskReactor
//
//  Created by Melanie Lislie Hsu on 8/21/15.
//  Copyright (c) 2015 Melanie Lislie Hsu. All rights reserved.
//

#import "TaskCollectionViewCell.h"

@implementation TaskCollectionViewCell

//Need edit and delete icons on each cell - ex. click to edit, swipe offscreen to delete

- (void)awakeFromNib {
    // Initialization code
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    self.taskName.text = textField.text;
}



@end
