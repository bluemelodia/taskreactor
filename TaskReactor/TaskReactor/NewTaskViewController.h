//
//  NewTaskViewController.h
//  TaskReactor
//
//  Created by Melanie Lislie Hsu on 8/23/15.
//  Copyright (c) 2015 Melanie Lislie Hsu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AADatePicker.h"
#import "CALayer+XibConfiguration.h"
#import "Chameleon.h"
#import <QuartzCore/QuartzCore.h>

@protocol TaskCreated <NSObject>

- (void)taskCreated;

@end

@interface NewTaskViewController : UIViewController <UITextFieldDelegate, AADatePickerDelegate, UICollectionViewDataSource, UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *backgroundView;
@property (nonatomic, strong) NSMutableArray *compColorPalette;
@property (nonatomic, strong) NSMutableArray *synColorPalette;
@property (weak, nonatomic) IBOutlet UITextField *titleField;
@property (weak, nonatomic) IBOutlet UIButton *dateField;
@property (weak, nonatomic) IBOutlet UIView *datePickerView;
@property (weak, nonatomic) IBOutlet UIButton *typeField;
@property (weak, nonatomic) IBOutlet UICollectionView *typeCollectionView;
@property (weak, nonatomic) IBOutlet UIButton *createTask;
@property (weak, nonatomic) IBOutlet UIImageView *launchScreen;
@property (weak, nonatomic) IBOutlet UIImageView *alertScreen;
@property (weak, nonatomic) IBOutlet UILabel *labelChoice;

@end
