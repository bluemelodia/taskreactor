//
//  TaskUtils.m
//  TaskReactor
//
//  Created by Melanie Lislie Hsu on 8/21/15.
//  Copyright (c) 2015 Melanie Lislie Hsu. All rights reserved.
//

#import "TaskUtils.h"

@implementation TaskUtils

// Reads a JSON file and returns the file data in the form of an NSDictionary
+ (NSDictionary*)readProductDocument:(NSString*)docName {
    NSString *filePath = [[NSBundle mainBundle] pathForResource:docName ofType:@"json"];
    NSData *fileData = [NSData dataWithContentsOfFile:filePath];
    if(fileData == nil) return nil;
    
    __autoreleasing NSError *error = nil;
    NSDictionary *results = [NSJSONSerialization JSONObjectWithData:fileData options:kNilOptions error:&error];
    NSLog(@"RESULTS: %@", error);
    return results;
}

@end
