//
//  NewTaskCollectionViewCell.h
//  TaskReactor
//
//  Created by Melanie Lislie Hsu on 8/24/15.
//  Copyright (c) 2015 Melanie Lislie Hsu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewTaskCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *cellImage;

- (void)setThumbnail:(UIImage *)image;
- (void)setCellName:(NSString *)name;
- (NSString *)getCellName;

@end
