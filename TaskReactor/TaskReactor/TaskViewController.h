//
//  TaskViewController.h
//  TaskReactor
//
//  Created by Melanie Lislie Hsu on 8/21/15.
//  Copyright (c) 2015 Melanie Lislie Hsu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Chameleon.h"

@interface TaskViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collView;
@property (weak, nonatomic) IBOutlet UINavigationBar *navBar;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segControl;
@property (weak, nonatomic) IBOutlet UIButton *plus;
@property (nonatomic, strong) NSMutableArray *colorPalette;

@end
