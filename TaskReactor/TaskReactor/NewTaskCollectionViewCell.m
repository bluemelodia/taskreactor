//
//  NewTaskCollectionViewCell.m
//  TaskReactor
//
//  Created by Melanie Lislie Hsu on 8/24/15.
//  Copyright (c) 2015 Melanie Lislie Hsu. All rights reserved.
//

#import "NewTaskCollectionViewCell.h"

@implementation NewTaskCollectionViewCell {
    UITapGestureRecognizer *imageTap;
    NSString *cellName;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setThumbnail:(UIImage *)image {
    [self.cellImage setImage:image];
}

- (void)setCellName:(NSString *)name {
    cellName = name;
}

- (NSString *)getCellName {
    return cellName;
}

@end
