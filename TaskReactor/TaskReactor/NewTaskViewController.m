//
//  NewTaskViewController.m
//  TaskReactor
//
//  Created by Melanie Lislie Hsu on 8/23/15.
//  Copyright (c) 2015 Melanie Lislie Hsu. All rights reserved.
//

#import "NewTaskViewController.h"
#import "NewTaskCollectionViewCell.h"
#import "TaskUtils.h"

@interface NewTaskViewController () {
    UIFont *customFont;
    AADatePicker *datePicker;
    
    // Borders for the date picker label
    CALayer *dateFieldTopBorder;
    CALayer *dateFieldLeftBorder;
    CALayer *dateFieldRightBorder;
    CALayer *dateFieldBottomBorder;
    
    // Borders for the date picker view
    CALayer *datePickerLeftBorder;
    CALayer *datePickerRightBorder;
    CALayer *datePickerBottomBorder;
    
    // Borders for the type field button
    CALayer *typeFieldTopBorder;
    CALayer *typeFieldLeftBorder;
    CALayer *typeFieldRightBorder;
    
    // Array holding the category images
    NSMutableArray *categoryImages;
    NSMutableArray *categoryColors;
    NSMutableArray *categoryNames;
}

@end

@implementation NewTaskViewController

@synthesize alertScreen, backgroundView, compColorPalette, createTask, dateField, datePickerView, labelChoice, launchScreen, synColorPalette, titleField, typeCollectionView, typeField;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    typeCollectionView.delegate = self;
    typeCollectionView.dataSource = self;
    
    // No need to set extra insets because of the navigation controller 
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    // Complementary colors to match the main interface, analogous colors for the current interface
    compColorPalette = [[NSMutableArray alloc] init];
    [compColorPalette addObjectsFromArray:[NSArray arrayOfColorsWithColorScheme:ColorSchemeComplementary with:self.navigationController.navigationBar.backgroundColor flatScheme:NO]];
    synColorPalette = [[NSMutableArray alloc] init];
    [synColorPalette addObjectsFromArray:[NSArray arrayOfColorsWithColorScheme:ColorSchemeAnalogous with:self.navigationController.navigationBar.backgroundColor flatScheme:NO]];
    
    // Register a custom font
    customFont = [UIFont fontWithName:@"Bend2SquaresBRK" size:28];
    
    titleField.layer.borderColor = [titleField.textColor CGColor];
    titleField.layer.borderWidth = 1.0f;
    
    if ([titleField respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        UIColor *color = [UIColor colorWithRed:204/255.0f green:102/255.0f blue:255/255.0f alpha:1.0f];
        titleField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Naming Is Hard (Max 100 Chars)" attributes:@{NSForegroundColorAttributeName:color}];
    }
    
    dateField.layer.borderColor = [[UIColor colorWithRed:102/255.0f green:204/255.0f blue:255/255.0f alpha:1.0] CGColor];
    dateField.layer.borderWidth = 1.0f;
    [dateField setTitleColor:[UIColor colorWithRed:102/255.0f green:204/255.0f blue:255/255.0f alpha:1.0]  forState:UIControlStateSelected];
    
    typeField.layer.borderColor = [[UIColor colorWithRed:255/255.0f green:128/255.0f blue:0/255.0f alpha:1.0] CGColor];
    typeField.layer.borderWidth = 1.0f;
    
    createTask.layer.borderColor = [[UIColor colorWithRed:255/255.0f green:0/255.0f blue:0/255.0f alpha:1.0] CGColor];
    createTask.layer.borderWidth = 1.0f;
    
    launchScreen.layer.masksToBounds = YES;
    launchScreen.layer.cornerRadius = 8.0;
    [launchScreen.layer setBorderColor: [[UIColor blackColor] CGColor]];
    [launchScreen.layer setBorderWidth: 10.0];
    
    alertScreen.layer.masksToBounds = YES;
    alertScreen.layer.cornerRadius = 8.0;
    [alertScreen.layer setBorderColor: [[UIColor blackColor] CGColor]];
    [alertScreen.layer setBorderWidth: 10.0];
    alertScreen.hidden = YES;
    
    labelChoice.layer.borderColor = [[UIColor colorWithRed:255/255.0f green:0/255.0f blue:128/255.0f alpha:1.0] CGColor];
    labelChoice.layer.borderWidth = 1.0f;
    
    // Initialize the CALayers
    dateFieldTopBorder = [CALayer layer];
    dateFieldLeftBorder = [CALayer layer];
    dateFieldRightBorder = [CALayer layer];
    dateFieldBottomBorder = [CALayer layer];
    
    datePickerLeftBorder = [CALayer layer];
    datePickerRightBorder = [CALayer layer];
    datePickerBottomBorder = [CALayer layer];
    
    typeFieldTopBorder = [CALayer layer];
    typeFieldLeftBorder = [CALayer layer];
    typeFieldRightBorder = [CALayer layer];
    
    // Add a custom date picker
    datePicker = [[AADatePicker alloc] initWithFrame:CGRectMake(0, 0, 290, datePickerView.frame.size.height) maxDate:[NSDate dateWithTimeIntervalSinceNow:5*365*24*60*60] minDate:[NSDate date] showValidDatesOnly:YES];
    datePicker.delegate = self;
    [datePickerView addSubview:datePicker];
    
    // Date Picker starts hidden
    datePickerView.hidden = YES;
    datePickerView.userInteractionEnabled = NO;
    
    // Register class for the category collection view
    UINib *cellNib = [UINib nibWithNibName:@"NewTaskCollectionViewCell" bundle:nil];
    [typeCollectionView registerNib:cellNib forCellWithReuseIdentifier:@"NewTaskCell"];
    
    // Event type collection view starts hidden
    typeCollectionView.hidden = YES;
    
    // Retrieve the category info
    NSDictionary *events = [TaskUtils readProductDocument:@"events"];
    NSDictionary *eventTypes = events[@"events"];
    NSLog(@"tasks: %lu", [eventTypes count]);
    
    categoryImages = [[NSMutableArray alloc] init];
    categoryColors = [[NSMutableArray alloc] init];
    categoryNames = [[NSMutableArray alloc] init];
    for (int i = 0; i < [eventTypes count]; i++) {
        [categoryImages addObject:[UIImage imageNamed:[[eventTypes valueForKey:@"icon"] objectAtIndex:i]]];
        [categoryNames addObject:[[eventTypes valueForKey:@"title"] objectAtIndex:i]];
        NSArray *background = [[eventTypes valueForKey:@"background"] objectAtIndex:i];
        CGFloat red = [[background objectAtIndex:0] floatValue];
        CGFloat green = [[background objectAtIndex:1] floatValue];
        CGFloat blue = [[background objectAtIndex:2] floatValue];
        UIColor *backgroundColor = [UIColor colorWithRed:red/255.0f green:green/255.0f blue:blue/255.0f alpha:1.0f];
        [categoryColors addObject:backgroundColor];
    }
    
    // TODO: try reading the info into either core data or parse
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

# pragma mark Text Field Methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [titleField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (range.length + range.location > titleField.text.length) {
        return NO;
    }
    NSUInteger newTextLength = [titleField.text length] + [string length] - range.length;
    return newTextLength <= 80;
}

- (void)deactivateTextField {
    [titleField resignFirstResponder];
}

# pragma mark Date Picker Methods
- (IBAction)onDateFieldButtonPress:(id)sender {
    [dateField setSelected:!dateField.selected];
    if (!dateField.selected) {
        datePickerView.hidden = YES;
        datePickerView.userInteractionEnabled = NO;
        [self setLayoutForDateFieldIdleState];
    } else {
        // Hide the collection view
        if (typeCollectionView.hidden == NO) {
            [self setLayoutForTypeFieldIdleState];
        }
        datePickerView.hidden = NO;
        datePickerView.userInteractionEnabled = YES;
        [self setLayoutForDateFieldSelectedState];
    }
}

- (void)setLayoutForDateFieldSelectedState {
    dateField.layer.borderColor = [[UIColor clearColor] CGColor];
    
    struct CGColor *dateTextColor = [[UIColor colorWithRed:102/255.0f green:204/255.0f blue:255/255.0f alpha:1.0] CGColor];
    
    // Add borders for the date field
    CGFloat dateFieldWidth = self.dateField.frame.size.width;
    CGFloat dateFieldHeight = self.dateField.frame.size.height;

    dateFieldTopBorder.frame = CGRectMake(0.0f, 0.0f, dateFieldWidth, 1.0f);
    dateFieldTopBorder.backgroundColor = dateTextColor;
    [dateField.layer addSublayer:dateFieldTopBorder];
    
    dateFieldLeftBorder.frame = CGRectMake(0.0f, 0.0f, 1.0f, dateFieldHeight);
    dateFieldLeftBorder.backgroundColor = dateTextColor;
    [dateField.layer addSublayer:dateFieldLeftBorder];
    
    dateFieldRightBorder.frame = CGRectMake(dateFieldWidth-1, 0.0f, 1.0f, dateFieldHeight);
    dateFieldRightBorder.backgroundColor = dateTextColor;
    [dateField.layer addSublayer:dateFieldRightBorder];
    
    dateFieldBottomBorder.frame = CGRectMake(15.0f, dateFieldHeight-1, dateFieldWidth-30, 1.0f);
    dateFieldBottomBorder.backgroundColor = dateTextColor;
    [dateField.layer addSublayer:dateFieldBottomBorder];
    
    // Add borders for the custom date picker
    CGFloat datePickerWidth = self.datePickerView.frame.size.width;
    CGFloat datePickerHeight = self.datePickerView.frame.size.height;
    
    datePickerLeftBorder.frame = CGRectMake(0.0f, 0.0f, 1.0f, datePickerHeight);
    datePickerLeftBorder.backgroundColor = dateTextColor;
    [datePickerView.layer addSublayer:datePickerLeftBorder];
    
    datePickerRightBorder.frame = CGRectMake(datePickerWidth-1, 0.0f, 1.0f, datePickerHeight);
    datePickerRightBorder.backgroundColor = dateTextColor;
    [datePickerView.layer addSublayer:datePickerRightBorder];
    
    datePickerBottomBorder.frame = CGRectMake(0.0f, datePickerHeight-1, datePickerWidth, 1.0f);
    datePickerBottomBorder.backgroundColor = dateTextColor;
    [datePickerView.layer addSublayer:datePickerBottomBorder];
}

- (void)setLayoutForDateFieldIdleState {
    dateField.layer.borderColor = [[UIColor colorWithRed:102/255.0f green:204/255.0f blue:255/255.0f alpha:1.0] CGColor];
}

# pragma mark Date Picker View Methods
//TODO: save the date upon valid submission into the array - send it back to TaskViewController along with the title
-(void)dateChanged:(AADatePicker *)sender {
    NSString *dateString = [NSDateFormatter localizedStringFromDate:sender.date
                                                          dateStyle:NSDateFormatterLongStyle
                                                          timeStyle:NSDateFormatterMediumStyle];
    [dateField setTitle:dateString forState:UIControlStateNormal | UIControlStateSelected];
}

# pragma mark Event Button Methods
- (IBAction)onEventButtonPress:(id)sender {
    [typeField setSelected:!typeField.selected];
    if (typeField.selected) {
        [self setLayoutForTypeFieldSelectedState];
    } else {
        [self setLayoutForTypeFieldIdleState];
    }
}

- (void)setLayoutForTypeFieldSelectedState {
    typeCollectionView.hidden = NO;
    typeField.layer.borderColor = [[UIColor clearColor] CGColor];
    
    // Add borders for the date field
    CGFloat typeFieldWidth = self.typeField.frame.size.width;
    CGFloat typeFieldHeight = self.typeField.frame.size.height;
    CGColorRef typeColor = [[UIColor colorWithRed:255/255.0f green:128/255.0f blue:0/255.0f alpha:1.0] CGColor];
    
    typeFieldTopBorder.frame = CGRectMake(0.0f, 0.0f, typeFieldWidth, 1.0f);
    typeFieldTopBorder.backgroundColor = typeColor;
    [typeField.layer addSublayer:typeFieldTopBorder];
    
    typeFieldLeftBorder.frame = CGRectMake(0.0f, 0.0f, 1.0f, typeFieldHeight);
    typeFieldLeftBorder.backgroundColor = typeColor;
    [typeField.layer addSublayer:typeFieldLeftBorder];
    
    typeFieldRightBorder.frame = CGRectMake(typeFieldWidth-1, 0.0f, 1.0f, typeFieldHeight);
    typeFieldRightBorder.backgroundColor = typeColor;
    [typeField.layer addSublayer:typeFieldRightBorder];
}

- (void)setLayoutForTypeFieldIdleState {
    typeField.layer.borderColor = [[UIColor colorWithRed:255/255.0f green:128/255.0f blue:0/255.0f alpha:1.0] CGColor];
    typeField.layer.borderWidth = 1.0f;
    typeCollectionView.hidden = YES;
}


# pragma mark Event Category Collection View Methods

// Set the cell size
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGSize cellSize;
    
    if (iDevice == iPhone) {
        cellSize = CGSizeMake(285/4, 285/4);
    } else {
        cellSize = CGSizeMake(screenWidth/3, screenWidth/3);
    }
    return cellSize;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 1;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 10;
}

// Set layout edges
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(15, 15, 15, 15);
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [categoryNames count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    NewTaskCollectionViewCell *cell = (NewTaskCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"NewTaskCell" forIndexPath:indexPath];

    // Configure the cell
    [cell setThumbnail:[categoryImages objectAtIndex:indexPath.row]];
    [cell setCellName:[categoryNames objectAtIndex:indexPath.row]];
    cell.backgroundColor = [categoryColors objectAtIndex:indexPath.row];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NewTaskCollectionViewCell *cell = (NewTaskCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    NSLog(@"CellName: %@", [cell getCellName]);
    [labelChoice setText:[NSString stringWithFormat:@"You Chose: %@", [cell getCellName]]];
    [self onEventButtonPress:self];
}


# pragma mark Cancel Task Creation

- (IBAction)onCancelTaskButton:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

# pragma mark Create Taks Methods

- (IBAction)onFireButtonPress:(id)sender {
    if ([titleField.text length] > 0) {
        NSLog(@"Success");
        NSString *title = titleField.text; // This field cannot be empty; the rest can
        NSString *category = labelChoice.text;
        //TODO: must extract the date from this, and check if it's actually a date
        NSDate *date = dateField.titleLabel.text;
        NSLog(@"Title: %@ Category: %@ Date: %@", title, category, date);
        // Call create new event function!
    } else {
        alertScreen.hidden = NO;
    }
}

@end
